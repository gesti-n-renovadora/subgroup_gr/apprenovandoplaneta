import React, {Component} from 'react';
import {
    Text,
    TextInput,
    View,
    StyleSheet,
    TouchableOpacity,
    TouchableHighlight,
    ImageBackground,
    Image,
    ScrollView
} from 'react-native';
import SelectInput from 'react-native-select-input-ios';

export default class FormFamiliar extends Component{
    constructor(){
        super()

        this.state = {
            nombres : '',
            apellidos : '',
            fechaNacimiento : '',
            genero : '',
            parentesco : '',
        }
    }

    /**Ir a la Siguiente Ventana*/
    navigateNext = (() => {
        //console.log(this.props.navigation);
        this.props.navigation.navigate("Reciclador");
    })

    changeNombres(nombres){
        this.setState({nombres})
    }
    changeApellidos(apellidos){
        this.setState({apellidos})
    }
    changeFechaNacimiento(fechaNacimiento){
        this.setState({fechaNacimiento})
    }
    changeGenero(genero){
        this.setState({genero})
    }
    changeParentesco(parentesco){
        this.setState({parentesco})
    }
    
    sexo() {
        return [
          {value: 0, label: '>>>'},
          {value: 1, label: 'Femenino'},
          {value: 2, label: 'Masculino'},
        ];
      }

      parent() {
        return [
            {value: 0, label: '>>>'},
            {value: 1, label: 'Espos@'},
            {value: 2, label: 'Compañer@'},
            {value: 3, label: 'Hij@'},
            {value: 4, label: 'Niet@'},
            {value: 5, label: 'Prim@'},
            {value: 6, label: 'Suegr@'},
            {value: 7, label: 'Padre'},
            {value: 8, label: 'Madre'},
            {value: 9, label: 'Yerno'},
            {value: 10, label: 'Nuera'},
            {value: 11, label: 'Herman@'},
            {value: 12, label: 'Abuel@'},
            {value: 13, label: 'Ti@'},
            {value: 14, label: 'Sobrin@'},
        ];
      }

      render(){
        return(
        <View style={styles.container}>
            <ScrollView>
            <View style={styles.containerInputs}>
                    <Text style={styles.textQuestion}>Nombres</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.nombres}
                        onChangeText={(nombres) => this.changeNombres (nombres)}
                    />
                    <Text style={styles.textQuestion}>Apellidos</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.apellidos}
                        onChangeText={(apellidos) => this.changeApellidos (apellidos)}
                    />
                    <Text style={styles.textQuestion}>Fecha de Nacimiento</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.fechaNacimiento}
                        onChangeText={(fechaNacimiento) => this.changeFechaNacimiento (fechaNacimiento)}
                    />
                    <Text style={styles.textQuestion}>Genero</Text>
                    <SelectInput style={styles.inputText}
                        options={this.sexo()}
                        value={this.state.genero}
                        onChangeText={(genero) => this.changeGenero (genero)}
                    />
                    <Text style={styles.textQuestion}>Parentesco</Text>
                    <SelectInput style={styles.inputText}
                        options={this.parent()}
                        value={this.state.parentesco}
                        onChangeText={(parentesco) => this.changeParentesco (parentesco)}
                    />
                    <View>
                    <TouchableHighlight
                    disabled={this.state.loading}
                    style={[styles.photoButton, styles.button]}
                    onPress={this.navigateNext}
                    >
                    <Text style={styles.textButton}>Registrar</Text>
                    </TouchableHighlight>
                    </View>
            </View>
            </ScrollView>
        </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white'
        
    },
    containerInputs: {
        marginTop: 10,
        marginBottom: 30,
        marginHorizontal: 10,
    },
    inputText: {
        borderWidth: 1,
        borderColor: '#ccc',
        paddingHorizontal: 5,
        paddingVertical: 5,
        
    },
    textQuestion: {
        fontWeight: 'bold' 
    },
    button: {
        backgroundColor: '#61987b',
        paddingVertical: 20,
        marginTop: 20,
        marginHorizontal: 100,
        borderRadius: 5,
        marginBottom: 10,
        borderWidth: 1,
        borderColor: '#ccc'   
    },
    textButton: {
        textAlign: 'center',
        color: 'white'
    }
})



