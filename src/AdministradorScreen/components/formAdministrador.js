import React, {Component} from 'react';
import {
    Text,
    TextInput,
    View,
    StyleSheet,
    TouchableOpacity,
    TouchableHighlight,
    ImageBackground,
    Image,
    ScrollView
} from 'react-native';
import SelectInput from 'react-native-select-input-ios';

export default class FormFamiliar extends Component{
    constructor(){
        super()

        this.state = {
            cedula : '',
            nombres : '',
            apellidos : '',
            direccion : '',
            celular : '',
            correo : '',
            administraOtrosCA: '',
            cantidadCA: '',
            tratamientoDatos: '',
        }
    }

    /**Ir a la Siguiente Ventana*/
    navigateNext = (() => {
        //console.log(this.props.navigation);
        this.props.navigation.navigate("Menú");
    })

    changeCedula(cedula){
        this.setState({cedula})
    }
    changeNombres(nombres){
        this.setState({nombres})
    }
    changeApellidos(apellidos){
        this.setState({apellidos})
    }
    changeDireccion(direccion){
        this.setState({direccion})
    }
    changeCelular(celular){
        this.setState({celular})
    }
    changeCorreo(correo){
        this.setState({correo})
    }
    changeAdministraOtrosCA(administraOtrosCA){
        this.setState({administraOtrosCA})
    }
    changeCantidadCA(cantidadCA){
        this.setState({catidadCA})
    }
    changeTratamientoDatos(tratamientoDatos){
        this.setState({tratamientoDatos})
    }

    desicion() {
        return [
          {value: 0, label: '>>>'},
          {value: 1, label: 'Si'},
          {value: 2, label: 'No'},
        ];
      }

      render(){
        return(
        <View style={styles.container}>
            <ScrollView>
            <View style={styles.containerInputs}>
                    <Text style={styles.textQuestion}>Cedula</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.cedula}
                        onChangeText={(cedula) => this.changeCedula (cedula)}
                    />
                    <Text style={styles.textQuestion}>Nombres</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.nombres}
                        onChangeText={(nombres) => this.changeNombres (nombres)}
                    />
                    <Text style={styles.textQuestion}>Apellidos</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.apellidos}
                        onChangeText={(apellidos) => this.changeApellidos (apellidos)}
                    />
                    <Text style={styles.textQuestion}>Dirección</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.direccion}
                        onChangeText={(direccion) => this.changeDireccion (direccion)}
                    />
                    <Text style={styles.textQuestion}>Celular</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.celular}
                        onChangeText={(celular) => this.changeCelular (celular)}
                    />
                    <Text style={styles.textQuestion}>Correo</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.correo}
                        onChangeText={(correo) => this.changeCorreo (correo)}
                    />
                    <Text style={styles.textQuestion}>¿Administra Otros CA?</Text>
                    <SelectInput style={styles.inputText}
                        options={this.desicion()}
                        value={this.state.administraOtrosCA}
                        onChangeText={(administraOtrosCA) => this.changeAdministraOtrosCA (administraOtrosCA)}
                    />
                    <Text style={styles.textQuestion}>Cantidad de CA que Administra</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.cantidadCA}
                        onChangeText={(cantidadCA) => this.changeCantidadCA (cantidadCA)}
                    />
                    <Text style={styles.textQuestion}>¿Autoriza el tratamiento de Datos?</Text>
                    <SelectInput style={styles.inputText}
                        options={this.desicion()}
                        value={this.state.tratamientoDatos}
                        onChangeText={(tratamientoDatos) => this.changeTratamientoDatos (tratamientoDatos)}
                    />
                    <View>
                    <View style={styles.containerButton}>    
                    <TouchableOpacity style={styles.buttonImage}
                    onPress={this.navigateFirma}>
                    <ImageBackground source={ require('./Lapiz.png')}  
                    style={styles.logo}/>
                    <Text style={styles.textButtonImage}>Firma</Text>
                    </TouchableOpacity>
                    </View>
                    <TouchableHighlight
                    disabled={this.state.loading}
                    style={[styles.photoButton, styles.button]}
                    onPress={this.navigateNext}
                    >
                    <Text style={styles.textButton}>Registrar</Text>
                    </TouchableHighlight>
                    </View>
            </View>
            </ScrollView>
        </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white'
        
    },
    logo: {
        flex: 1,
    },
    containerInputs: {
        marginTop: 10,
        marginBottom: 30,
        marginHorizontal: 10,
    },
    inputText: {
        borderWidth: 1,
        borderColor: '#ccc',
        paddingHorizontal: 5,
        paddingVertical: 5,
        
    },
    textQuestion: {
        fontWeight: 'bold' 
    },
    button: {
        backgroundColor: '#61987b',
        paddingVertical: 20,
        marginTop: 20,
        marginHorizontal: 100,
        borderRadius: 5,
        marginBottom: 10,
        borderWidth: 1,
        borderColor: '#ccc'   
    },
    buttonImage: {
        flex: 1,
        height: 150,
        width: 130,
        justifyContent: 'center',
        backgroundColor: 'white',
        paddingVertical: 5,
        marginHorizontal: 5,
        borderRadius: 10,
        marginBottom: 5,
        borderWidth: 1,
        borderColor: '#ccc'   
    },
    containerButton: {
        flexDirection: 'row',
        marginTop: 10,
        marginBottom: 5,
        marginHorizontal: 110,
        paddingHorizontal: 15,
        alignItems: 'center'
    },
    textButtonImage: {
        textAlign: 'center',
        justifyContent: 'flex-end',
        color: 'black',
        backgroundColor: 'transparent'
    },
    textButton: {
        textAlign: 'center',
        color: 'white'
    }
})



