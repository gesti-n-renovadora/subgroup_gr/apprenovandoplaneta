import React, {Component} from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    TouchableHighlight,
    ImageBackground,
    Image,
} from 'react-native';
import FormReciclador from './components/formReciclador';
//import Botones from './components/botones'


export class Reciclador extends Component{

    /**Ir a la Siguiente Ventana*/
    navigateNext = (() => {
        this.props.navigation.navigate("Login");
    })

    render(){
        return(
            <View style={styles.container}>
            <ImageBackground
                source={require('../../assets/header.png')}
                style={styles.logo} 
            >
            <View style={styles.containerButton0}>
                <TouchableOpacity style={styles.buttonCerrarSesion}
                onPress={this.navigateNext}>
                    <Image source={ require('../../assets/btncerrarsesion.png')}  
                    style={styles.logo0}/>
                </TouchableOpacity>
            </View>
            <Text style={styles.textTitle}>Reciclador</Text>
            </ImageBackground>
            
            <FormReciclador/>
            
            </View>
        );
    }
}

    
const styles = StyleSheet.create({
    container:{
        flex: 1    
    },
    logo: {
        height: 108,
        width: 420
    },
    logo0: {
        height: 30,
        width: 30
    },
    logoButton: {
        flex: 1,
    },
    textTitle: {
        marginTop: 75,
        textAlign: 'center',
        justifyContent: 'flex-end',
        color: '#61987b',
        fontSize: 20,
        fontWeight: 'bold'
    },
    buttonCerrarSesion: {
        height: 30,
        width: 30,
        marginBottom: 5,
        justifyContent: 'center',
        backgroundColor: 'white',
        
    },
    button: {
        backgroundColor: '#61987b',
        paddingVertical: 20,
        marginTop: 20,
        marginHorizontal: 100,
        borderRadius: 5,
        marginBottom: 10,
        borderWidth: 1,
        borderColor: '#ccc'   
    },
    containerButton: {
        flexDirection: 'row',
        //marginTop: 130,
        marginBottom: 5,
        marginHorizontal: 30,
        paddingHorizontal: 15,
        
    },
    buttonImage: {
        height: 170,
        width: 150,
        justifyContent: 'center',
        backgroundColor: 'white',
        paddingVertical: 5,
        marginHorizontal: 5,
        borderRadius: 10,
        marginBottom: 5,
        borderWidth: 1,
        borderColor: '#ccc'   
    },
    containerButton0: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginTop: 5,
        marginHorizontal: 15,
        
    },
    textButtonImage: {
        textAlign: 'center',
        justifyContent: 'flex-end',
        color: 'black',
        backgroundColor: 'transparent'
    } 
})
    
export default Reciclador;