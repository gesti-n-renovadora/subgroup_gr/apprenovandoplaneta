import React, {Component} from 'react';
import {
    Text,
    TextInput,
    View,
    StyleSheet,
    TouchableOpacity,
    TouchableHighlight,
    ImageBackground,
    Image,
    ScrollView
} from 'react-native';
import SelectInput from 'react-native-select-input-ios';
import { Form, TextValidator } from 'react-native-validator-form';

export default class FormCA extends Component{
    constructor(){
        super()

        this.state = {
            codigo : '',
            nombre : '',
            nit : '',
            direccion : '',
            celular : '',
            correo : '',
            cantidadUnidades: '',
            estrato: '',
            barrioComuna: '',
            reciclador: '',
            tiempoReciclador: '',
            cuentaAgua: '',
            cuentaLuz: '',
            volumenResiduos: '',
            capacitaciones: '',
            areaTema: '',
            certificacion: '',
            requerimiento: '',
            tiempoAcuerdo: '',
            observaciones: '',
            administrador: '',
            _reciclador: '',
        }
    }

    /**Ir a la Siguiente Ventana*/
    navigateNext = (() => {
        //console.log(this.props.navigation);
        this.props.navigation.navigate("Menú");
    })

    changeCodigo(codigo){
        this.setState({codigo})
    }
    changeNombre(nombre){
        this.setState({nombre})
    }
    changeNit(nit){
        this.setState({nit})
    }
    changeDireccion(direccion){
        this.setState({direccion})
    }
    changeCelular(celular){
        this.setState({celular})
    }
    changeCorreo(correo){
        this.setState({correo})
    }
    changeCantidadUnidades(cantidadUnidades){
        this.setState({cantidadUnidades})
    }
    changeEstrato(estrato){
        this.setState({estrato})
    }
    changeBarrioComuna(barrioComuna){
        this.setState({barrioComuna})
    }
    changeReciclador(reciclador){
        this.setState({reciclador})
    }
    changeTiempoReciclador(tiempoReciclador){
        this.setState({tiempoReciclador})
    }
    changeCuentaAgua(cuentaAgua){
        this.setState({cuentaAgua})
    }
    changeCuentaAgua(cuentaLuz){
        this.setState({cuentaLuz})
    }
    changeVolumenResiduos(volumenResiduos){
        this.setState({volumenResiduos})
    }
    changeCapacitaciones(capacitaciones){
        this.setState({capacitaciones})
    }
    changeAreaTema(areaTema){
        this.setState({areaTema})
    }
    changeCapacitaciones(certificacion){
        this.setState({certificacion})
    }
    changeRequerimiento(requerimiento){
        this.setState({requerimiento})
    }
    changeTiempoAcuerdo(tiempoAcuerdo){
        this.setState({tiempoAcuerdo})
    }
    changeObservaciones(observaciones){
        this.setState({observaciones})
    }
    changeAdministrador(administrador){
        this.setState({administrador})
    }
    change_Reciclador(_reciclador){
        this.setState({_reciclador})
    }

    _estrato() {
        return [
          {value: 0, label: '>>>'},
          {value: 1, label: '1'},
          {value: 2, label: '2'},
          {value: 3, label: '3'},
          {value: 4, label: '4'},
          {value: 5, label: '5'},
          {value: 6, label: '6'},
        ];
      }

    _barrioComuna() {
        return [
            {value:0, label: '>>>'},
            {value:1, label: 'ASENTAMIENTO EL MILAGRO/1',},
            {value:2, label: 'BARRIO ARENALES/1'},
            {value:3, label: 'BARRIO ARRAYANES/1'},
            {value:4, label: 'BARRIO EL EMPERADOR/1'},
            {value:5, label: 'BARRIO LA ISABELA/1'},
            {value:6, label: 'BARRIO PINARES/1'},
            {value:7, label: 'SECTOR ESTADIO CENTENARIO/1'},
            {value:8, label: 'SECTOR JARDINES/1'},
            {value:9, label: 'SECTOR SANTA ANA/1'},
            {value:10, label: 'SECTOR TRES ESQUINAS/1'},
            {value:11, label: 'URBANIZACION AGUAZUL/1'},
            {value:12, label: 'URBANIZACION BAMBUSA/1'},
            {value:13, label: 'URBANIZACION BARU/1'},
            {value:14, label: 'URBANIZACION BOSQUES DE PINARES/1'},
            {value:15, label: 'URBANIZACION CABO DE LA VELA/1'},
            {value:16, label: 'URBANIZACION CANAS GORDAS/1'},
            {value:17, label: 'URBANIZACION CANO CRISTALES/1'},
            {value:18, label: 'URBANIZACION CASTILLA GRANDE/1'},
            {value:19, label: 'URBANIZACION CIUDADELA SIMON BOLIVAR/1'},
            {value:20, label: 'URBANIZACION EL PALMAR/1'},
            {value:21, label: 'URBANIZACION GENESIS/1'},
            {value:22, label: 'URBANIZACION GUADUALES DE LA VILLA/1'},
            {value:23, label: 'URBANIZACION GUADUALES DEL EDEN/1'},
            {value:24, label: 'URBANIZACION LA ARCADIA/1'},
            {value:25, label: 'URBANIZACION LA CASTILLA/1'},
            {value:26, label: 'URBANIZACION LA LINDA/1'},
            {value:27, label: 'URBANIZACION NUESTRA SENORA DE LA PAZ/1'},
            {value:28, label: 'URBANIZACION PORTAL DE PINARES/1'},
            {value:29, label: 'URBANIZACION PORTAL DEL EDEN/1'},
            {value:30, label: 'URBANIZACION VILLA DEL CENTENARIO/1'},
            {value:31, label: 'URBANIZACION VILLA MARAVELES/1'},
            {value:32, label: 'URBANIZACION VISTA HERMOSA/1'},
            {value:33, label: 'ASENTAMIENTO 25 DE ENERO/2'},
            {value:34, label: 'ASENTAMIENTO EL RECUERDO/2'},
            {value:35, label: 'ASENTAMIENTO LAS VERANERAS/2'},
            {value:36, label: 'BARRIO 14 DE OCTUBRE/2'},
            {value:37, label: 'BARRIO 19 DE ENERO/2'},
            {value:38, label: 'BARRIO 8 DE MARZO/2'},
            {value:39, label: 'BARRIO ANTONIO NARINO/2'},
            {value:40, label: 'BARRIO BELLO HORIZONTE/2'},
            {value:41, label: 'BARRIO BOSQUES DE GIBRALTAR/2'},
            {value:42, label: 'BARRIO BOYACA/2'},
            {value:43, label: 'BARRIO CALIMA/2'},
            {value:44, label: 'BARRIO FARALLONES/2'},
            {value:45, label: 'BARRIO GIBRALTAR/2'},
            {value:46, label: 'BARRIO JESUS MARIA OCAMPO/2'},
            {value:47, label: 'BARRIO LA BRASILIA/2'},
            {value:48, label: 'BARRIO LA MILAGROSA/2'},
            {value:49, label: 'BARRIO LAS ACACIAS/2'},
            {value:50, label: 'BARRIO LOS NARANJOS/2'},
            {value:51, label: 'BARRIO LOS QUINDOS/2'},
            {value:52, label: 'BARRIO MARCO FIDEL SUAREZ/2'},
            {value:53, label: 'BARRIO PATRICIA/2'},
            {value:54, label: 'BARRIO SAN VICENTE DE PAUL/2'},
            {value:55, label: 'BARRIO SANTA RITA/2'},
            {value:56, label: 'BARRIO ZULDEMAYDA/2'},
            {value:57, label: 'SECTOR EL MIRADOR/2'},
            {value:58, label: 'URBANIZACION ALCAZAR DEL CAF?/2'},
            {value:59, label: 'URBANIZACION ALEJANDRIA/2'},
            {value:60, label: 'URBANIZACION BRASILIA NUEVA/2'},
            {value:61, label: 'URBANIZACION CIUDADELA EL POBLADO/2'},
            {value:62, label: 'URBANIZACION CIUDADELA NUEVO HORIZONTE/2'},
            {value:63, label: 'URBANIZACION CIUDADELA PUERTO ESPEJO/2'},
            {value:64, label: 'URBANIZACION CRISTALES/2'},
            {value:65, label: 'URBANIZACION EL CARMELO/2'},
            {value:66, label: 'URBANIZACION EL REFUGIO/2'},
            {value:67, label: 'URBANIZACION EL TESORITO/2'},
            {value:68, label: 'URBANIZACION LA FACHADA/2'},
            {value:69, label: 'URBANIZACION LA VIRGINIA/2'},
            {value:70, label: 'URBANIZACION LAS BRISAS/2'},
            {value:71, label: 'URBANIZACION LAS SERRANIAS/2'},
            {value:72, label: 'URBANIZACION LINDARAJA/2'},
            {value:73, label: 'URBANIZACION LOS GIRASOLES/2'},
            {value:74, label: 'URBANIZACION LUIS CARLOS GALAN SARMIENTO/2'},
            {value:75, label: 'URBANIZACION MANANTIALES/2'},
            {value:76, label: 'URBANIZACION MARBELLA/2'},
            {value:77, label: 'URBANIZACION SAN FRANCISCO/2'},
            {value:78, label: 'URBANIZACION TRES ESQUINAS/2'},
            {value:79, label: 'URBANIZACION VERACRUZ/2'},
            {value:80, label: 'URBANIZACION VILLA ALEJANDRA/2'},
            {value:81, label: 'URBANIZACION VILLA CLAUDIA/2'},
            {value:82, label: 'URBANIZACION VILLA DE LA VIDA Y EL TRABAJO/2'},
            {value:83, label: 'URBANIZACION VILLA DEL CAFÉ/2'},
            {value:84, label: 'URBANIZACION VILLA DEL CARMEN/2'},
            {value:85, label: 'URBANIZACION VILLA ITALIA/2'},
            {value:86, label: 'URBANIZACION VILLA JULIANA/2'},
            {value:87, label: 'BARRIO 25 DE MAYO/3'},
            {value:88, label: 'BARRIO ALFONSO LOPEZ/3'},
            {value:89, label: 'BARRIO BELEN/3'},
            {value:90, label: 'BARRIO COOPERATIVO/3'},
            {value:91, label: 'BARRIO EL PLACER/3'},
            {value:92, label: 'BARRIO LA ADIELA/3'},
            {value:93, label: 'BARRIO LA MIRANDA/3'},
            {value:94, label: 'BARRIO LOTEROS/3'},
            {value:95, label: 'BARRIO MANUELA BELTRAN/3'},
            {value:96, label: 'BARRIO SANTA HELENA/3'},
            {value:97, label: 'BARRIO SANTA MARIA/3'},
            {value:98, label: 'SECTOR SINAI/3'},
            {value:99, label: 'URBANIZACION 13 DE JUNIO/3'},
            {value:100, label: 'URBANIZACION ARCO IRIS/3'},
            {value:101, label: 'URBANIZACION BOSQUES DE VIENA/3'},
            {value:102, label: 'URBANIZACION CASABLANCA/3'},
            {value:103, label: 'URBANIZACION CIUDAD DORADA/3'},
            {value:104, label: 'URBANIZACION CIUDADELA EL SOL/3'},
            {value:105, label: 'URBANIZACION CIUDADELA LAS COLINAS/3'},
            {value:106, label: 'URBANIZACION CIUDADELA NUEVA ARMENIA/3'},
            {value:107, label: 'URBANIZACION EL SINAI/3'},
            {value:108, label: 'URBANIZACION LA ALHAMBRA/3'},
            {value:109, label: 'URBANIZACION LA CECILIA/3'},
            {value:110, label: 'URBANIZACION LA CRISTALINA/3'},
            {value:111, label: 'URBANIZACION LA ESMERALDA/3'},
            {value:112, label: 'URBANIZACION LA GRECIA/3'},
            {value:113, label: 'URBANIZACION LA RIVERA/3'},
            {value:114, label: 'URBANIZACION LOMA VERDE/3'},
            {value:115, label: 'URBANIZACION LOMAS DE LA UNION/3'},
            {value:116, label: 'URBANIZACION LOS ARCADES/3'},
            {value:117, label: 'URBANIZACION LOS KIOSCOS/3'},
            {value:118, label: 'URBANIZACION MONTECARLO/3'},
            {value:119, label: 'URBANIZACION NUEVO AMANECER/3'},
            {value:120, label: 'URBANIZACION QUINTAS DE LA MARINA/3'},
            {value:121, label: 'URBANIZACION SAN DIEGO/3'},
            {value:122, label: 'URBANIZACION VILLA ANGELA/3'},
            {value:123, label: 'URBANIZACION VILLA ANGELA_CORDILLERA/3'},
            {value:124, label: 'URBANIZACION VILLA HERMOSA/3'},
            {value:125, label: 'URBANIZACION VILLA LAURA/3'},
            {value:126, label: 'BARRIO 7 DE AGOSTO/4'},
            {value:127, label: 'BARRIO BELENCITO/4'},
            {value:128, label: 'BARRIO BERLIN/4'},
            {value:129, label: 'BARRIO CINCUENTENARIO/4'},
            {value:130, label: 'BARRIO EL PORVENIR/4'},
            {value:131, label: 'BARRIO EL PRADO/4'},
            {value:132, label: 'BARRIO EL RECREO/4'},
            {value:133, label: 'BARRIO GAITAN/4'},
            {value:134, label: 'BARRIO LA ANUNCIACION/4'},
            {value:135, label: 'BARRIO LA UNION/4'},
            {value:136, label: 'BARRIO LAS MARGARITAS/4'},
            {value:137, label: 'BARRIO MIRAFLORES/4'},
            {value:138, label: 'BARRIO MONTEPRADO/4'},
            {value:139, label: 'BARRIO MONTEVIDEO/4'},
            {value:140, label: 'BARRIO NUEVA LIBERTAD/4'},
            {value:141, label: 'BARRIO OBRERO/4'},
            {value:142, label: 'BARRIO POPULAR/4'},
            {value:143, label: 'BARRIO SALAZAR/4'},
            {value:144, label: 'BARRIO SANTAFE/4'},
            {value:145, label: 'BARRIO SANTANDER/4'},
            {value:146, label: 'URBANIZACION EL SILENCIO/4'},
            {value:147, label: 'URBANIZACION LA MONTANA/4'},
            {value:148, label: 'URBANIZACION LOS ARTESANOS/4'},
            {value:149, label: 'URBANIZACION NUEVO RECREO/4'},
            {value:150, label: 'URBANIZACION PALMARES DEL RECREO/4'},
            {value:151, label: 'URBANIZACION PRIMERO DE MAYO/4'},
            {value:152, label: 'URBANIZACION VILLA LILIANA/4'},
            {value:153, label: 'BARRIO 7 DE AGOSTO/5'},
            {value:154, label: 'BARRIO BERLIN/5'},
            {value:155, label: 'BARRIO EL RECREO/5'},
            {value:156, label: 'BARRIO EL SILENCIO/5'},
            {value:157, label: 'BARRIO LA ANUNCIACION/5'},
            {value:158, label: 'BARRIO LA MONTANA/5'},
            {value:159, label: 'BARRIO LA UNION/5'},
            {value:160, label: 'BARRIO LAS MARGARITAS/5'},
            {value:161, label: 'BARRIO MONTEPRADO/5'},
            {value:162, label: 'BARRIO MONTEVIDEO/5'},
            {value:163, label: 'BARRIO NUEVA LIBERTAD/5'},
            {value:164, label: 'BARRIO SALAZAR/5'},
            {value:165, label: 'URBANIZACION LOS ARTESANOS/5'},
            {value:166, label: 'URBANIZACION NUEVO RECREO/5'},
            {value:167, label: 'URBANIZACION PALMARES DEL RECREO/5'},
            {value:168, label: 'URBANIZACION PRIMERO DE MAYO/5'},
            {value:169, label: 'URBANIZACION VILLA LILIANA/5'},
            {value:170, label: 'BARRIO EL BOSQUE/6'},
            {value:171, label: 'BARRIO LA CLARITA/6'},
            {value:172, label: 'BARRIO LAS AMERICAS/6'},
            {value:173, label: 'BARRIO LOS CAMBULOS/6'},
            {value:174, label: 'BARRIO QUINDIO/6'},
            {value:175, label: 'BARRIO SAN ANDRES/6'},
            {value:176, label: 'BARRIO SAN JOSE/6'},
            {value:177, label: 'SECTOR QUEBRADA LA CLARITA-SAN JOSE/6'},
            {value:178, label: 'URBANIZACION ALTOS DE LA PAVONA/6'},
            {value:179, label: 'URBANIZACION CHALETS DE MONTEBLANCO/6'},
            {value:180, label: 'URBANIZACION CIUDADELA LA PATRIA/6'},
            {value:181, label: 'URBANIZACION CIUDADELA LOS ANDES/6'},
            {value:182, label: 'URBANIZACION EL CORTIJO/6'},
            {value:183, label: 'URBANIZACION EL PARQUE/6'},
            {value:184, label: 'URBANIZACION LA IRLANDA/6'},
            {value:185, label: 'URBANIZACION LA PAVONA/6'},
            {value:186, label: 'URBANIZACION LOS ALMENDROS/6'},
            {value:187, label: 'URBANIZACION MONTEBLANCO/6'},
            {value:188, label: 'URBANIZACION PROVIVIENDA UNIVERSAL/6'},
            {value:189, label: 'URBANIZACION QUINTAS DE JULIANA/6'},
            {value:190, label: 'URBANIZACION QUINTAS DE LOS ANDES/6'},
            {value:191, label: 'URBANIZACION ROJAS PINILLA_1/6'},
            {value:192, label: 'URBANIZACION ROJAS PINILLA_2/6'},
            {value:193, label: 'URBANIZACION SANTA SOFIA/6'},
            {value:194, label: 'URBANIZACION VENECIA/6'},
            {value:195, label: 'URBANIZACION VILLA ANDREA/6'},
            {value:196, label: 'URBANIZACION VILLA CAROLINA/6'},
            {value:197, label: 'URBANIZACION VILLA CELMIRA/6'},
            {value:198, label: 'URBANIZACION VILLA DE LAS AMERICAS/6'},
            {value:199, label: 'URBANIZACION VILLA JARDIN/6'},
            {value:200, label: 'URBANIZACION VILLA XIMENA/6'},
            {value:201, label: 'BARRIO BUENOS AIRES/7'},
            {value:202, label: 'BARRIO BUENOS AIRES BAJO/7'},
            {value:203, label: 'BARRIO BUENOS AIRES PLANO/7'},
            {value:204, label: 'BARRIO EL BOSQUE/7'},
            {value:205, label: 'BARRIO ESPANA/7'},
            {value:206, label: 'BARRIO GUAYAQUIL/7'},
            {value:207, label: 'BARRIO PATIO BONITO/7'},
            {value:208, label: 'BARRIO PATIO BONITO BAJO/7'},
            {value:209, label: 'BARRIO RINCON SANTO/7'},
            {value:210, label: 'BARRIO SAN FERNANDO/7'},
            {value:211, label: 'BARRIO SAN NICOLAS/7'},
            {value:212, label: 'BARRIO URIBE/7'},
            {value:213, label: 'BARRIO VELEZ/7'},
            {value:214, label: 'SECTOR CENTRO/7'},
            {value:215, label: 'SECTOR LA ALDEA/7'},
            {value:216, label: 'SECTOR LA FLORIDA/7'},
            {value:217, label: 'SECTOR LA MARIA/7'},
            {value:218, label: 'SECTOR PARQUE CAFETERO/7'},
            {value:219, label: 'SECTOR PARQUE VALENCIA/7'},
            {value:220, label: 'SECTOR SAN NICOLAS/7'},
            {value:221, label: 'URBANIZACION MARIA CRISTINA/7'},
            {value:222, label: 'BARRIO COLINAS CENTRO/8'},
            {value:223, label: 'BARRIO CORBONES/8'},
            {value:224, label: 'BARRIO EL PARAISO/8'},
            {value:225, label: 'BARRIO LA ESPERANZA/8'},
            {value:226, label: 'BARRIO LAS MERCEDES/8'},
            {value:227, label: 'BARRIO LOS LIBERTADORES/8'},
            {value:228, label: 'BARRIO PIAMONTE/8'},
            {value:229, label: 'BARRIO TIGREROS/8'},
            {value:230, label: 'URBANIZACION AHITAMARA/8'},
            {value:231, label: 'URBANIZACION 1/8'},
            {value:232, label: 'URBANIZACION EL JUBILEO/8'},
            {value:233, label: 'URBANIZACION EL LIMONAR/8'},
            {value:234, label: 'URBANIZACION FRANCISCO JOSE DE CALDAS/8'},
            {value:235, label: 'URBANIZACION LA DIVISA/8'},
            {value:236, label: 'URBANIZACION NUEVO BERLIN/8'},
            {value:237, label: 'URBANIZACION TERRANOVA EL ALBA/8'},
            {value:238, label: 'URBANIZACION VILLA INGLESA/8'},
            {value:239, label: 'URBANIZACION VILLA MARCELA/8'},
            {value:240, label: 'URBANIZACION VILLA SOFIA/8'},
            {value:241, label: 'ASENTAMIENTO VIEJA LIBERTAD/9'},
            {value:242, label: 'BARRIO GRANADA/9'},
            {value:243, label: 'BARRIO LA ARBOLEDA/9'},
            {value:244, label: 'BARRIO LA CABANA/9'},
            {value:245, label: 'BARRIO LAS PALMAS/9'},
            {value:246, label: 'BARRIO LAS SESENTA CASAS/9'},
            {value:247, label: 'BARRIO LOS ALAMOS/9'},
            {value:248, label: 'BARRIO MODELO/9'},
            {value:249, label: 'BARRIO NIAGARA/9'},
            {value:250, label: 'SECTOR EL COLISEO/9'},
            {value:251, label: 'SECTOR YULIMA/9'},
            {value:252, label: 'URBANIZACION EL REMANSO/9'},
            {value:253, label: 'URBANIZACION LA GRAN BRETANA/9'},
            {value:254, label: 'URBANIZACION MURANO/9'},
            {value:255, label: 'URBANIZACION NUEVO SOL/9'},
            {value:256, label: 'URBANIZACION SARA/9'},
            {value:257, label: 'ASENTAMIENTO LOS FUNDADORES/10'},
            {value:258, label: 'ASENTAMIENTO PUERTO RICO/10'},
            {value:259, label: 'ASENTAMIENTO SALVADOR ALLENDE/10'},
            {value:260, label: 'BARRIO ALCAZAR/10'},
            {value:261, label: 'BARRIO EL NOGAL/10'},
            {value:262, label: 'BARRIO GALAN/10'},
            {value:263, label: 'BARRIO LA CAMPINA/10'},
            {value:264, label: 'BARRIO LA CASTELLANA/10'},
            {value:265, label: 'BARRIO LA LORENA/10'},
            {value:266, label: 'BARRIO LAURELES/10'},
            {value:267, label: 'BARRIO LOS FUNDADORES/10'},
            {value:268, label: 'BARRIO LOS PROFESIONALES/10'},
            {value:269, label: 'BARRIO NUEVA CECILIA/10'},
            {value:270, label: 'BARRIO PROVIDENCIA/10'},
            {value:271, label: 'SECTOR ABORIGENES/10'},
            {value:272, label: 'SECTOR AV CENTENARIO/10'},
            {value:273, label: 'SECTOR BAVARIA/10'},
            {value:274, label: 'SECTOR COINCA/10'},
            {value:275, label: 'SECTOR CRQ/10'},
            {value:276, label: 'SECTOR LA FOGATA/10'},
            {value:277, label: 'SECTOR MONTERREDONDO/10'},
            {value:278, label: 'SECTOR NORTE AVENIDA 19/10'},
            {value:279, label: 'SECTOR PROVITEQ/10'},
            {value:280, label: 'SECTOR REGIVIT/10'},
            {value:281, label: 'SECTOR SAN JUAN/10'},
            {value:282, label: 'SECTOR TIGREROS/10'},
            {value:283, label: 'URBANIZACION ALAMEDA DEL NORTE/10'},
            {value:284, label: 'URBANIZACION LA MARIELA/10'},
            {value:285, label: 'URBANIZACION LOS SAUCES/10'},
            {value:286, label: 'URBANIZACION MERCEDES DEL NORTE/10'},
        ];
      }

    desicion() {
        return [
          {value: 0, label: '>>>'},
          {value: 1, label: 'Si'},
          {value: 2, label: 'No'},
        ];
    }
    
    _capacitaciones() {
        return [
          {value: 0, label: '>>>'},
          {value: 1, label: 'Separación en la fuente'},
          {value: 3, label: 'PGIRS'},
          {value: 4, label: 'Aprovechamiento'},
          {value: 5, label: 'Ninguna'},
          {value: 6, label: 'Otra'},
        ];
    }

    _requerimientos() {
        return [
          {value: 0, label: '>>>'},
          {value: 1, label: 'Puntos ecológicos'},
          {value: 3, label: 'Adecuación cuarto de basuras'},
          {value: 4, label: 'Campañas separación en la fuente'},
          {value: 5, label: 'Dotación implementos de aseo'},
          {value: 6, label: 'Ninguno'},
          {value: 7, label: 'Otros requerimientos'},
        ];
    }

    _administrador() {
        return [
          {value: 0, label: '>>>'},
          {value: 1, label: 'Administrador 1'},
          {value: 2, label: 'Administrador 2'},
        ];
    }

    _reciclador() {
        return [
          {value: 0, label: '>>>'},
          {value: 1, label: 'Reciclador 1'},
          {value: 2, label: 'Reciclador 2'},
        ];
    }

      render(){
        return(
        <View style={styles.container}>
            <ScrollView>
            <View style={styles.containerInputs}>
                    <Text style={styles.textQuestion}>Código</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.codigo}
                        onChangeText={(codigo) => this.changeCodigo (codigo)}
                    />

                    <Text style={styles.textQuestion}>Nombre / Razón social</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.nombre}
                        onChangeText={(nombre) => this.changeNombre (nombre)}
                    />

                    <Text style={styles.textQuestion}>NIT</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.nit}
                        onChangeText={(nit) => this.changeNit (nit)}
                    />

                    <Text style={styles.textQuestion}>Dirección</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.direccion}
                        onChangeText={(direccion) => this.changeDireccion (direccion)}
                    />

                    <Text style={styles.textQuestion}>Celular</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.celular}
                        onChangeText={(celular) => this.changeCelular (celular)}
                    />

                    <Text style={styles.textQuestion}>Correo</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.correo}
                        onChangeText={(correo) => this.changeCorreo (correo)}
                    />

                    <Text style={styles.textQuestion}>Cantidad Unidades</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.cantidadUnidades}
                        onChangeText={(cantidadUnidades) => this.changeCantidadUnidades (cantidadUnidades)}
                    />

                    <Text style={styles.textQuestion}>Estrato</Text>
                    <SelectInput style={styles.inputText}
                        options={this._estrato()}
                        value={this.state.estrato}
                        onChangeText={(estrato) => this.changeEstrato (estrato)}
                    />
                    
                    <Text style={styles.textQuestion}>Barrio/Comuna</Text>
                    <SelectInput style={styles.inputText}
                        options={this._barrioComuna()}
                        value={this.state.barrioComuna}
                        onChangeText={(barrioComuna) => this.changeBarrioComuna (barrioComuna)}
                    />

                    <Text style={styles.textQuestion}>¿Reciclador vinculado Permanentemente?</Text>
                    <SelectInput style={styles.inputText}
                        options={this.desicion()}
                        value={this.state.reciclador}
                        onChangeText={(reciclador) => this.changeReciclador (reciclador)}
                    />

                    <Text style={styles.textQuestion}>Tiempo que lleva el Recicldor con el CA (Meses)</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.tiempoReciclador}
                        onChangeText={(tiempoReciclador) => this.changeTiempoReciclador (tiempoReciclador)}
                    />

                    <Text style={styles.textQuestion}>Cuenta Acueducto</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.cuentaAgua}
                        onChangeText={(cuentaAgua) => this.changeCuentaAgua (cuentaAgua)}
                    />

                    <Text style={styles.textQuestion}>Cuenta Energía</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.cuentaLuz}
                        onChangeText={(cuentaLuz) => this.changeCuentaLuz (cuentaLuz)}
                    />

                    <Text style={styles.textQuestion}>Volumen Residuos (Mt³)</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.volumenResiduos}
                        onChangeText={(volumenResiduos) => this.changeVolumenResiduos (volumenResiduos)}
                    />

                    <Text style={styles.textQuestion}>¿Esta interesado en recibir Capacitaciones y/o Asesorías?</Text>
                    <SelectInput style={styles.inputText}
                        options={this._capacitaciones()}
                        value={this.state.capacitaciones}
                        onChangeText={(capacitaciones) => this.changeCapacitaciones (capacitaciones)}
                    />

                    <Text style={styles.textQuestion}>¿En qué Área o Tema?</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.areaTema}
                        onChangeText={(areaTema) => this.changeAreaTema (areaTema)}
                    />

                    <Text style={styles.textQuestion}>¿Requiere Certificación?</Text>
                    <SelectInput style={styles.inputText}
                        options={this.desicion()}
                        value={this.state.certificacion}
                        onChangeText={(certificacion) => this.changeCertificacion (certificacion)}
                    />

                    <Text style={styles.textQuestion}>¿Algún otro Requerimiento?</Text>
                    <SelectInput style={styles.inputText}
                        options={this._requerimientos()}
                        value={this.state.requerimiento}
                        onChangeText={(requerimiento) => this.changeRequerimiento (requerimiento)}
                    />

                    <Text style={styles.textQuestion}>Tiempo del acuerdo (Meses)</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.tiempoAcuerdo}
                        onChangeText={(tiempoAcuerdo) => this.changeTiempoAcuerdo (tiempoAcuerdo)}
                    />

                    <Text style={styles.textQuestion}>Observaciones Adicionales</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.observaciones}
                        multiline={true}
                        onChangeText={(observaciones) => this.changeObservaciones (observaciones)}
                    />

                    <Text style={styles.textQuestion}>Administrador</Text>
                    <SelectInput style={styles.inputText}
                        options={this._administrador()}
                        value={this.state.administrador}
                        onChangeText={(administrador) => this.changeAdministrador (administrador)}
                    />

                    <Text style={styles.textQuestion}>Reciclador</Text>
                    <SelectInput style={styles.inputText}
                        options={this._reciclador()}
                        value={this.state._reciclador}
                        onChangeText={(_reciclador) => this.change_Reciclador (_reciclador)}
                    />

                    <View>
                    <View style={styles.containerButton}>    
                    <TouchableOpacity style={styles.buttonImage}
                    onPress={this.navigateFirma}>
                    <ImageBackground source={ require('./accionesCA.png')}  
                    style={styles.logo}/>
                    <Text style={styles.textButtonImage}>Acciones CA</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.buttonImage}
                    onPress={this.navigateFirma}>
                    <ImageBackground source={ require('./accionesGE.png')}  
                    style={styles.logo}/>
                    <Text style={styles.textButtonImage}>Acciones GE</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.buttonImage}
                    onPress={this.navigateFirma}>
                    <ImageBackground source={ require('./materiales.png')}  
                    style={styles.logo}/>
                    <Text style={styles.textButtonImage}>Materiales</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.buttonImage}
                    onPress={this.navigateFirma}>
                    <ImageBackground source={ require('./contactos.png')}  
                    style={styles.logo}/>
                    <Text style={styles.textButtonImage}>Contactos</Text>
                    </TouchableOpacity>
                    </View>
                    <TouchableHighlight
                    disabled={this.state.loading}
                    style={[styles.photoButton, styles.button]}
                    onPress={this.navigateNext}
                    >
                    <Text style={styles.textButton}>Registrar</Text>
                    </TouchableHighlight>
                    </View>
            </View>
            </ScrollView>
        </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white'
        
    },
    logo: {
        flex: 1,
    },
    containerInputs: {
        marginTop: 10,
        marginBottom: 30,
        marginHorizontal: 10,
    },
    inputText: {
        borderWidth: 1,
        borderColor: '#ccc',
        paddingHorizontal: 5,
        paddingVertical: 5,
        
    },
    textQuestion: {
        fontWeight: 'bold' 
    },
    button: {
        backgroundColor: '#61987b',
        paddingVertical: 20,
        marginTop: 20,
        marginHorizontal: 100,
        borderRadius: 5,
        marginBottom: 10,
        borderWidth: 1,
        borderColor: '#ccc'   
    },
    buttonImage: {
        //flex: 1,
        height: 110,
        width: 88,
        justifyContent: 'center',
        backgroundColor: 'white',
        paddingVertical: 5,
        marginHorizontal: 5,
        borderRadius: 10,
        marginBottom: 5,
        borderWidth: 1,
        borderColor: '#ccc'   
    },
    containerButton: {
        flexDirection: 'row',
        marginTop: 10,
        marginBottom: 5,
        //marginHorizontal: 1,
        //paddingHorizontal: 15,
        alignItems: 'center'
    },
    textButtonImage: {
        textAlign: 'center',
        justifyContent: 'flex-end',
        color: 'black',
        backgroundColor: 'transparent',
        fontSize: 12,
    },
    textButton: {
        textAlign: 'center',
        color: 'white'
    }
})



