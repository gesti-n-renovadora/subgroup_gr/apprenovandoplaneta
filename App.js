import * as React from 'react';
import { Button, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Login from './src/LoginScreen/login';
import Menu from './src/MenuScreen/menu.js';
import Reciclador from './src/RecicladorScreen/reciclador';
import Administrador from './src/AdministradorScreen/Administrador';
import Familiar from './src/FamiliarScreen/Familiar';
import CA from './src/ClientesScreen/components/formCA'

const AppNavigator = createStackNavigator();

function MiAppNavigator() {
  return (
    <AppNavigator.Navigator>
      <AppNavigator.Screen 
      name="Login" 
      component={Login}
      options={{
        title: "",
        headerStyle: {
          backgroundColor: '#61987B',
        },
      }}
      />
      <AppNavigator.Screen name="Menú" component={Menu}/>
      <AppNavigator.Screen name="Reciclador" component={Reciclador}/>
      <AppNavigator.Screen name="Administrador" component={Administrador}/>
      <AppNavigator.Screen name="Familiar" component={Familiar}/>
      <AppNavigator.Screen name="CA" component={CA}
      options={{
        title: "Cliente Agrupado",
        headerStyle: {
          backgroundColor: '#61987B',
        },
      }}/>
    </AppNavigator.Navigator>
  );
}

export default function App() {
  return (
    <NavigationContainer>
      <MiAppNavigator /> 
    </NavigationContainer>
  );
}