import React, {Component} from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    TouchableHighlight,
    ImageBackground,
    StyleSheet,
} from 'react-native';

/**Ir a la Siguiente Ventana*/
navigateFamiliar = (() => {
    this.props.navigation.navigate("Familiar");
    })

const Botones = (
    <View style={styles.containerButton}>
            <TouchableOpacity style={styles.buttonImage}
            /*onPress={this.navigateFirma}*/>
                <ImageBackground source={ require('./Lapiz.png')}  
                style={styles.logo}/>
                <Text style={styles.textButtonImage}>Firma</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.buttonImage}
            onPress={this.navigateFamiliar}>
                <ImageBackground source={ require('./Familia.png')}  
                style={styles.logo}/>
                <Text style={styles.textButtonImage}>Familiares</Text>
            </TouchableOpacity>
    </View>
);

const styles = StyleSheet.create({
    containerButton: {
        flexDirection: 'row',
        marginBottom: 5,
        marginHorizontal: 110,
        paddingHorizontal: 15,
        alignItems: 'center'
        
    },
    buttonImage: {
        height: 85,
        width: 75,
        justifyContent: 'center',
        backgroundColor: 'white',
        paddingVertical: 5,
        marginHorizontal: 5,
        borderRadius: 10,
        marginBottom: 5,
        borderWidth: 1,
        borderColor: '#ccc'   
    },
    logo: {
        flex: 1,
    },
    textButtonImage: {
        textAlign: 'center',
        justifyContent: 'flex-end',
        color: 'black',
        backgroundColor: 'transparent'
    }
})

export default Botones;