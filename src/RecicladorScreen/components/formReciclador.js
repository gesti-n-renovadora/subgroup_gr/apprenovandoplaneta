import React, {Component} from 'react';
import {
    Text,
    TextInput,
    View,
    StyleSheet,
    TouchableOpacity,
    TouchableHighlight,
    ImageBackground,
    Image,
    ScrollView
} from 'react-native';
import SelectInput from 'react-native-select-input-ios';

export default class FormReciclador extends Component{

    /**Ir a la Siguiente Ventana*/
    navigateFamiliar = (() => {
    this.props.navigation.navigate("Familiar");
    })

    /**Ir a la Siguiente Ventana*/
    navigateRegistrar = (() => {
        this.props.navigation.navigate("Menú");
        })

    constructor(){
        super()

        this.state = {
            cedula : '',
            nombres : '',
            apellidos : '',
            direccion : '',
            celular : '',
            correo : '',
            estadoCivil : '',
            cantidadHijos : '',
            cuentaRut : '',
            manejaOtrosCa : '',
            cantidadCa: '',
            fuenteMaterialRc: '',
            eps: '',
            materialVehiculo: '',
            vendeMaterial: '',
            ventaMaterial: '',
            quienVende: '',
            capacitacion: '',
            areaTema:'',
            requerimiento:'',
            observaciones: '',
            tratamientoDatos:'',
            
        }
    }

    changeCedula(cedula){
        this.setState({cedula})
    }
    changeNombres(nombres){
        this.setState({nombres})
    }
    changeApellidos(apellidos){
        this.setState({apellidos})
    }
    changeDireccion(direccion){
        this.setState({direccion})
    }
    changeCelular(celular){
        this.setState({celular})
    }
    changeCorreo(correo){
        this.setState({correo})
    }
    changeEstadoCivil(estadoCivil){
        this.setState({estadoCivil})
    }
    changeCantidadHijos(cantidadHijos){
        this.setState({cantidadHijos})
    }
    changeCuentaRut(cuentaRut){
        this.setState({cuentaRut})
    }
    changeManejaOtrosCa(manejaOtrosCa){
        this.setState({manejaOtrosCa})
    }
    changeCantidadCa(cantidadCa){
        this.setState({cantidadCa})
    }
    changeFuenteMaterialRc(fuenteMaterialRc){
        this.setState({fuenteMaterialRc})
    }
    changeEps(eps){
        this.setState({eps})
    }
    changeMaterialVehiculo(materialVehiculo){
        this.setState({materialVehiculo})
    }
    changeVendeMaterial(vendeMaterial){
        this.setState({vendeMaterial})
    }
    changeVentaMaterial(ventaMaterial){
        this.setState({ventaMaterial})
    }
    changeQuienVende(quienVende){
        this.setState({quienVende})
    }
    changePagoMaterial(pagoMaterial){
        this.setState({pagoMaterial})
    }
    changeCapacitacion(capacitacion){
        this.setState({capacitacion})
    }
    changeAreaTema(areaTema){
        this.setState({areaTema})
    }
    changeRequerimiento(requerimiento){
        this.setState({requerimiento})
    }
    changeObservaciones(observaciones){
        this.setState({observaciones})
    }
    changeTratamientoDatos(tratamientoDatos){
        this.setState({tratamientoDatos})
    }

    desicion() {
        return [
          {value: 0, label: '>>>'},
          {value: 1, label: 'Si'},
          {value: 2, label: 'No'},
        ];
      }

      eCivil() {
        return [
            {value: 0, label: '>>>'},
            {value: 1, label: 'Solter@'},
            {value: 2, label: 'Casad@'},
            {value: 3, label: 'Separad@'},
            {value: 4, label: 'Divorciad@'},
            {value: 5, label: 'Unión Libre'},
            {value: 6, label: 'Viud@'},
        ];
      }

      entidadSalud() {
        return [
            {value: 0, label: '>>>'},
            {value: 1, label: 'Coomeva'},
            {value: 2, label: 'Cruz Blanca'},
            {value: 3, label: 'Famisanar'},
            {value: 4, label: 'Humana Vivir'},
            {value: 5, label: 'Medimas'},
            {value: 6, label: 'Nueva EPS'},
            {value: 7, label: 'Salud Total'},
            {value: 8, label: 'Salud Coop'},
            {value: 9, label: 'Salud Vida'},
            {value: 10, label: 'Sanitas'},
            {value: 11, label: 'SISBEN'},
            {value: 12, label: 'SOS'},
            {value: 13, label: 'Sura EPS'},
            {value: 14, label: 'Ninguna'},
        ];
      }

      pagoMaterial() {
        return [
            {value: 0, label: '>>>'},
            {value: 1, label: 'El mismo día'},
            {value: 2, label: 'Semanal'},
            {value: 3, label: 'Quincenal'},
            {value: 4, label: 'Mensual'},
            {value: 5, label: 'Otro'},
        ];
      }

      capacitacion() {
        return [
            {value: 0, label: '>>>'},
            {value: 1, label: 'Separación en la fuente'},
            {value: 2, label: 'PGIRS'},
            {value: 3, label: 'Aprovechamiento'},
            {value: 4, label: 'Ninguna'},
            {value: 5, label: 'Otra'},
        ];
      }

      requerimiento() {
        return [
            {value: 0, label: '>>>'},
            {value: 1, label: 'Implementos de Seguridad'},
            {value: 2, label: 'Mejor pago del Material'},
            {value: 3, label: 'Ninguno'},
            {value: 4, label: 'Otro'},
        ];
      }



    render(){
        return(
        <View style={styles.container}>
            <ScrollView>
            <View style={styles.containerInputs}>
                    <Text style={styles.textQuestion}>Cédula</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.cedula}
                        onChangeText={(cedula) => this.changeCedula (cedula)}
                    />
                    <Text style={styles.textQuestion}>Nombres</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.nombres}
                        onChangeText={(nombres) => this.changeNombres (nombres)}
                    />
                    <Text style={styles.textQuestion}>Apellidos</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.apellidos}
                        onChangeText={(apellidos) => this.changeApellidos (apellidos)}
                    />
                    <Text style={styles.textQuestion}>Dirección</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.direccion}
                        onChangeText={(direccion) => this.changeDireccion (direccion)}
                    />
                    <Text style={styles.textQuestion}>Celular</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.celular}
                        onChangeText={(celular) => this.changeCelular (celular)}
                    />
                    <Text style={styles.textQuestion}>Correo</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.correo}
                        onChangeText={(correo) => this.changeCorreo (correo)}
                    />
                    <Text style={styles.textQuestion}>Estado Civil</Text>
                    <SelectInput style={styles.inputText}
                        options={this.eCivil()}
                        value={this.state.estadoCivil}
                        onChangeText={(estadoCivil) => this.changeEstadoCivil (estadoCivil)}
                    />
                    <Text style={styles.textQuestion}>Cantidad de Hijos</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.cantidadHijos}
                        onChangeText={(cantidadHijos) => this.changeCantidadHijos (cantidadHijos)}
                    />
                    <Text style={styles.textQuestion}>¿Cuenta con RUT?</Text>
                    <SelectInput style={styles.inputText}
                        options={this.desicion()}
                        value={this.state.cuentaRut}
                        onChangeText={(cuentaRut) => this.changeCuentaRut (cuentaRut)}
                    />
                    <Text style={styles.textQuestion}>¿Maneja otros CA?</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.manejaOtrosCa}
                        onChangeText={(manejaOtrosCa) => this.changeManejaOtrosCa (manejaOtrosCa)}
                    />
                    <Text style={styles.textQuestion}>¿Cantidad de CA donde recoge?</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.cantidadCa}
                        onChangeText={(cantidadCa) => this.changeCantidadCa (cantidadCa)}
                    />
                    <Text style={styles.textQuestion}>¿Su única fuente de ingresos es el material RC?</Text>
                    <SelectInput style={styles.inputText}
                        options={this.desicion()}
                        value={this.state.fuenteMaterialRc}
                        onChangeText={(fuenteMaterialRc) => this.changeFuenteMaterialRc (fuenteMaterialRc)}
                    />
                    <Text style={styles.textQuestion}>¿Cuál es su EPS?</Text>
                    <SelectInput style={styles.inputText}
                        options={this.entidadSalud()}
                        value={this.state.eps}
                        onChangeText={(eps) => this.changeEps (eps)}
                    />
                    <Text style={styles.textQuestion}>¿Cuenta con vehículo para transportar el material?</Text>
                    <SelectInput style={styles.inputText}
                        options={this.desicion()}
                        value={this.state.materialVehiculo}
                        onChangeText={(materialVehiculo) => this.changeMaterialVehiculo (materialVehiculo)}
                    />
                    <Text style={styles.textQuestion}>¿Vende el material el mismo día que lo recoge?</Text>
                    <SelectInput style={styles.inputText}
                        options={this.desicion()}
                        value={this.state.vendeMaterial}
                        onChangeText={(vendeMaterial) => this.changeVendeMaterial (vendeMaterial)}
                    />
                    <Text style={styles.textQuestion}>¿Días en que vende el material?</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.ventaMaterial}
                        onChangeText={(ventaMaterial) => this.changeVentaMaterial (ventaMaterial)}
                    />
                    <Text style={styles.textQuestion}>¿A quien le vende el material?</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.quienVende}
                        onChangeText={(quienVende) => this.changeQuienVende (quienVende)}
                    />
                    <Text style={styles.textQuestion}>¿Cada cuánto le pagan el material?</Text>
                    <SelectInput style={styles.inputText}
                        options={this.pagoMaterial()}
                        value={this.state.pagoMaterial}
                        onChangeText={(pagoMaterial) => this.changePagoMaterial (pagoMaterial)}
                    />
                    <Text style={styles.textQuestion}>¿Qué capacitaciones y/o asesorias, esta interesado en recibir?</Text>
                    <SelectInput style={styles.inputText}
                        options={this.capacitacion()}
                        value={this.state.capacitacion}
                        onChangeText={(capacitacion) => this.changeCapacitacion (capacitacion)}
                    />
                    <Text style={styles.textQuestion}>¿En qué Área o Tema?</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.areaTema}
                        onChangeText={(areaTema) => this.changeAreaTema (areaTema)}
                    />
                    <Text style={styles.textQuestion}>¿Algún otro Requerimiento?</Text>
                    <SelectInput style={styles.inputText}
                        options={this.requerimiento()}
                        value={this.state.requerimiento}
                        onChangeText={(requerimiento) => this.changeRequerimiento (requerimiento)}
                    />
                    <Text style={styles.textQuestion}>Observaciones Adicionales</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.observaciones}
                        multiline={true}
                        onChangeText={(observaciones) => this.changeObservaciones (observaciones)}
                    />
                    <Text style={styles.textQuestion}>¿Autoriza el tratamiento de datos?</Text>
                    <SelectInput style={styles.inputText}
                        options={this.desicion()}
                        placeholder="Seleccione"
                        value={this.state.tratamientoDatos}
                        onChangeText={(tratamientoDatos) => this.changeTratamientoDatos (tratamientoDatos)}
                    />
            </View>
            <View style={styles.containerButton}>
            <TouchableOpacity style={styles.buttonImage}
            onPress={this.navigateFirma}>
                <ImageBackground source={ require('./Lapiz.png')}  
                style={styles.logo}/>
                <Text style={styles.textButtonImage}>Firma</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.buttonImage}
            onPress={this.navigateFamiliar}>
                <ImageBackground source={ require('./Familia.png')}  
                style={styles.logo}/>
                <Text style={styles.textButtonImage}>Familiares</Text>
            </TouchableOpacity>
            </View>

            <View />
            <TouchableHighlight
                    disabled={this.state.loading}
                    style={styles.buttonReg}
                    onPress={this.navigateRegistrar}
                    //onPress={this.navigateNext}
                    >
                    <Text style={styles.textButton}>Registrar</Text>
            </TouchableHighlight>
            
            </ScrollView>
        </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white'
    },
    logo: {
        flex: 1,
    },
    containerInputs: {
        marginTop: 10,
        marginBottom: 30,
        marginHorizontal: 10,
    },
    inputText: {
        borderWidth: 1,
        borderColor: '#ccc',
        paddingHorizontal: 5,
        paddingVertical: 5,
        
    },
    textQuestion: {
        fontWeight: 'bold' 
    },
    button: {
        backgroundColor: '#61987b',
        paddingVertical: 20,
        marginTop: 20,
        marginHorizontal: 100,
        borderRadius: 5,
        marginBottom: 10,
        borderWidth: 1,
        borderColor: '#ccc'   
    },
    buttonImage: {
        flex: 1,
        height: 95,
        width: 130,
        justifyContent: 'center',
        backgroundColor: 'white',
        paddingVertical: 5,
        marginHorizontal: 5,
        borderRadius: 10,
        marginBottom: 5,
        borderWidth: 1,
        borderColor: '#ccc'   
    },
    containerButton: {
        flexDirection: 'row',
        marginBottom: 5,
        marginHorizontal: 110,
        paddingHorizontal: 15,
        alignItems: 'center'
    },
    buttonReg: {
        backgroundColor: '#61987b',
        paddingVertical: 20,
        marginHorizontal: 100,
        borderRadius: 5,
        marginBottom: 10,
        borderWidth: 1,
        borderColor: '#ccc'   
    },
    textButton: {
        textAlign: 'center',
        color: 'white'
    },
    textButtonImage: {
        textAlign: 'center',
        justifyContent: 'flex-end',
        color: 'black',
        backgroundColor: 'transparent'
    }
})



